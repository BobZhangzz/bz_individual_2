# bz_individual_2
In this project, we are building a simple Rust API and containerize it using docker

[demo video](https://youtu.be/fsw2acXO8IE)

## How to run the project
1. main.rs contains the main code for the API

2. Dockerfile contains the docker configuration for the API

3. To run the project, you need to have docker installed on your machine

4. Clone the project to your local machine

5. Navigate to the project directory

6. Run the following command to build the docker image
```bash
docker build -t rust-api .
```

7. Run the following command to run the docker container
```bash
docker run -p 8000:8080 rust-api
```

8. The API will be available at http://localhost:8000

9. You can test the API by sending a GET request to http://localhost:8000

10. You should see the following response
```json
{
  "message": "Hello, World!"
}
```
### Building and running your application

When you're ready, start your application by running:
`docker compose up --build`.

Your application will be available at http://localhost:8080.

### Deploying your application to the cloud

First, build your image, e.g.: `docker build -t myapp .`.
If your cloud uses a different CPU architecture than your development
machine (e.g., you are on a Mac M1 and your cloud provider is amd64),
you'll want to build the image for that platform, e.g.:
`docker build --platform=linux/amd64 -t myapp .`.

Then, push it to your registry, e.g. `docker push myregistry.com/myapp`.

Consult Docker's [getting started](https://docs.docker.com/go/get-started-sharing/)
docs for more detail on building and pushing.

### References
* [Docker's Rust guide](https://docs.docker.com/language/rust/)


## screenshot
![Alt text](./img/watch.png)

![Alt text](./img/docker.png)

![Alt text](./img/cargotest.png)

